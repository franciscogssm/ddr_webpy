let button = {
    "select": 0,
    "start": 3,
    "x": 14,
    "ball": 13,
    "square": 15,
    "triangle": 12,

    "up": 4,
    "heart": 4,

    "down": 6,
    "sun": 6,

    "left": 7,
    "bear": 7,

    "right": 5,
    "flower": 5
}

begin();

function begin(){
    wait4(button.start, function () {
        console.log("level 1");
        levelOne();
    });
}

function wait4(someButton, callback) {
    let buttonPressed = false;
    let socket = io();

    socket.emit('message', 'teste');

    socket.on('button pressed', function (btn) {
        if (!buttonPressed) {
            if (btn == someButton) {
                buttonPressed = true;
            }
            else {
                turnSound('xx-miss', 'on');
            }
        }
        console.log(btn);
    });
    socket.on('button released', function (btn) {
        if (btn == someButton && buttonPressed) {
            socket.close();
            turnSound('xx-hit', 'on', function(){
                callback();
            });
        }
    })
}

function levelOne() {
    turnSound('01-intro', 'on', function () {
        wait4(button.heart, function () {
            console.log("coracao");
            turnSound('07-xStep', 'on', function () {
                wait4(button.x, function () {
                    console.log("x");
                    turnSound('08-sunStep', 'on', function(){
                        wait4(button.sun, function(){
                            turnSound('04-level2', 'on', function(){
                                levelTwo();
                            })
                        })
                    })
                })
            })
        })
    })
}

function levelTwo(){
    wait4(button.triangle, function(){
        turnSound('13-squaresStep', 'on', function(){
            wait4(button.square, function(){
                turnSound('14-bearStep', 'on', function(){
                    wait4(button.bear, function(){
                        turnSound('07-level3', 'on', function(){
                            levelThree();
                        })
                    })
                })
            })
        })
    })
}

function levelThree(){
    wait4(button.ball, function(){
        turnSound('19-flowerStep', 'on', function(){
            wait4(button.flower, function(){
                turnSound('09-endGame', 'on', function(){
                    begin();
                })
            })
        })
    })
}