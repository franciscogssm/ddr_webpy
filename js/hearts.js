function appendHeartBG() {
    class Heart extends mojs.CustomShape {
        getShape() {
            return '<path d="m49.109714,24.23141c20.034994,-57.477384 98.532758,0 0,73.899494c-98.532758,-73.899494 -20.034994,-131.376878 0,-73.899494z""></path>';
        }
    };

    mojs.addShape('heart', Heart);

    const heart = new mojs.Shape({
        shape: 'heart',
        fill: '#ff69b4',
        stroke: '#ff69cd',
        strokeWidth: 10, 
        isShowStart: true,
        scale: .9
    });


    const heartStroke1 = new mojs.Shape({
        shape: 'heart',
        fill: 'transparent',
        stroke: '#ff69c1',
        strokeWidth: {
            2: 3
        },
        duration: 1000,
        scale: {
            1.1: 1.15
        },
        isShowStart: true,
        isYoyo: true,
        repeat: 999
    }).play();


    const heartStroke2 = new mojs.Shape({
        shape: 'heart',
        fill: 'transparent',
        stroke: '#ff69cd',
        strokeWidth: {
            1: 2
        },
        delay: 100,
        duration: 900,
        scale: {
            1.15: 1.3
        },
        isShowStart: true,
        isYoyo: true,
        repeat: 999
    }).play();


    const heartAnimation = {
        scale: {
            .9: 1
        },
        duration: 300,
        easing: 'cubic.out'
    };


    document.addEventListener("click", () => {
        heart.
        then(heartAnimation).
        play();
        heartStroke1.play();
        heartStroke2.play();
    });
}