function turnSound(sound, onOff, callback = null ,autoOff = true) {

    let soundElement = document.getElementById(sound);

    if (onOff.includes("on")) {

        soundElement.load();

        soundElement.autoplay = true;

        if (autoOff) {
            setTimeout(function () {
                soundElement.load();
                if(callback)
                    callback();
            }, (500 + 1000 * soundElement.duration));
        }
        else{
            if(callback)
                callback();
        }

    } else {
        soundElement.autoplay = false;
        soundElement.load();
        if(callback)
            callback();
    }
}